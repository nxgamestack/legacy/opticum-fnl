(fn widget [a]
  (local self {})
  (local a (or a {}))

  ;name of widget
  (set self.name a.name)
  ;group of widget
  (set self.group a.group)

  ;size and location of the widget
  (set self.transform
    { :x (or a.x 10)
      :y (or a.y 10)
      :w (or a.w 10)
      :h (or a.h 10)})

  ;theme of the widget
  (set self.theme (or a.theme ((require :opticum.theme))))
  (set self.shadow false)

  ;can the user interact with this element
  (set self.enabled true)
  ;is the element selectable
  (set self.selectable false)
  ;is the element clickable
  (set self.clickable true)
  ;is the element selected
  (set self.selected false)
  ;is the user hovering over the element
  (set self.hovered false)
  ;is the element clicked
  (set self.clicked false)

  ;user has selected the element
  (fn self.on-selected [])
  ;user has hovered over the element
  (fn self.on-hovered [])
  ;user has clicked on the element
  (fn self.on-clicked [])


  (fn self.point-inside-p [p])

  (fn self.on-update [dt])

  (fn self.on-draw [])

  (fn self.on-event-mousepressed [e]
    (when (= e.button 1)
      (if (and (not e.consumed) self.hovered)
          (do
            (when self.selectable (set self.selected true))
            (when self.clickable (set self.clicked true))
            true)
          (and (not self.hovered))
          (when self.selectable (set self.selected false))
          (not e.consumed)
          (when self.clickable
            (set self.clicked self.hovered)
            self.hovered)
          false)))

  (fn self.on-event-mousereleased [e]
    (when (= e.button 1)
      (if (and (not e.consumed) self.hovered self.clicked)
          (do
            (when self.clickable
              ;x y button
              (self.on-clicked (- self.transform.x e.x) (- self.transform.y e.y))
              (set self.clicked false))
            true)
          (set self.clicked false))))

  (fn self.on-event-mousemoved [e]
    (set self.hovered (and (not e.consumed) (self.point-inside-p [e.x e.y])))
    self.hovered)

  (fn self.on-event-mousedragged [e])

  (fn self.on-event-textinput [e])
  (fn self.on-event-keypressed [e])

  ;(fn self.on-event- [e])

  (fn self.on-event [e])
    ;(when (and (= e.kind :mousepressed) (= e.button 1))
    ;  (if (and self.selectable self.hovered))))

  self)
